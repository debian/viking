#ifndef __RESOURCE_vik_icons_H__
#define __RESOURCE_vik_icons_H__

#include <gio/gio.h>

extern GResource *vik_icons_get_resource (void);

extern void vik_icons_register_resource (void);
extern void vik_icons_unregister_resource (void);

#endif
